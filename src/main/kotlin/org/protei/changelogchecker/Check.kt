package org.protei.changelogchecker

import java.nio.file.Files
import java.nio.file.Paths
import kotlin.streams.asSequence

/**
 * Created by dmitriy_varygin on 01.05.17.
 */
internal fun check(settings: CheckerPluginExt) {
    if (!checkVersionPropsExists(settings.pathToVersionProps)) {
        return
    }

    val version = getVersion(settings.pathToVersionProps)
    if (!version.isRelease()) {
        println("Version suffix contains ${version.suffix}. Changelog check will not be performed.")
        return
    }

    if (!checkChangelogExists(settings)) {
        return
    }

    if (!version.valid()) {
        println("Invalid version: $version. Can't parse it.")
        return
    }

    checkChangelogContainsVersionDescription(settings, version)
}

internal data class Version(var major: Int? = null, var middle: Int? = null,
                            var minor: Int? = null, var suffix: String? = null) {
    internal fun valid(): Boolean = null !in setOf(major, middle, minor)
    internal fun isRelease(): Boolean = suffix.isNullOrBlank()
    internal fun versionReleaseStr(): String = "$major.$middle.$minor"
}

private fun checkVersionPropsExists(pathToVersionProps: String): Boolean {
    return if (Files.exists(Paths.get(pathToVersionProps))) {
        true
    } else {
        println("Can't find version.properties in path: $pathToVersionProps")
        false
    }
}

private fun checkChangelogExists(settings: CheckerPluginExt): Boolean {
    val pathToChangelog = Paths.get(settings.pathToChangelog)
    if (!Files.exists(pathToChangelog)) {
        if (settings.crashOnAbsence) {
            throw CheckChangelogException("Can't find changelog in path: $pathToChangelog")
        } else {
            println("Can't find changelog in path: $pathToChangelog")
            return false
        }
    }
    return true
}

internal fun getVersion(pathToVersionProps: String): Version {
    val version = Version()
    Files.lines(Paths.get(pathToVersionProps)).forEach {
        when {
            "major=" in it -> version.major = Integer.parseInt(it.substringAfter("major="))
            "middle=" in it -> version.middle = Integer.parseInt(it.substringAfter("middle="))
            "minor=" in it -> version.minor = Integer.parseInt(it.substringAfter("minor="))
            "suffix=" in it -> version.suffix = it.substringAfter("suffix=")
        }
    }
    return version
}

internal fun checkChangelogContainsVersionDescription(settings: CheckerPluginExt, version: Version) {
    var currentVersionFound = false
    var validDelimiterFound = false
    var descriptionFound = false
    run breaker@ {
        Files.lines(Paths.get(settings.pathToChangelog)).asSequence().forEach {
            if (validDelimiterFound) {
                if (settings.descriptionRegex.toRegex() !in it) {
                    reportViolation(settings.crashOnAbsence, "Next line after delimiter after ${version.versionReleaseStr()} " +
                            "must contain valid description, used regex: ${settings.descriptionRegex}.")
                } else {
                    descriptionFound = true
                }
                return@breaker
            }
            if (currentVersionFound) {
                if ("---" !in it) {
                    reportViolation(settings.crashOnAbsence, "Next line after ${version.versionReleaseStr()} must contain at least 3 '-'.")
                }
                validDelimiterFound = true
            }
            if (version in it) {
                currentVersionFound = true
            }
            if (!currentVersionFound && versionRegex.matches(it)) {
                reportViolation(settings.crashOnAbsence, "The current release version should be the first in the " +
                        "${settings.pathToChangelog}. It should be at the top. Now $it at the top ${settings.pathToChangelog}.")
            }
        }
    }

    if (descriptionFound) {
        println("Found valid description for ${version.versionReleaseStr()} in ${settings.pathToChangelog}.")
    } else {
        reportViolation(settings.crashOnAbsence,
                "In ${settings.pathToChangelog} can't find description of ${version.versionReleaseStr()} version.")
    }
}

private val versionRegex = "\\s*\\d+\\.\\d+\\.\\d+\\s*".toRegex()

private fun reportViolation(crashOnAbsence: Boolean, message: String) {
    if (crashOnAbsence) {
        throw CheckChangelogException(message)
    } else {
        println(message)
    }
}

internal operator fun String.contains(version: Version): Boolean {
    val releaseStr = version.versionReleaseStr()
    return releaseStr in this && "\\w*$releaseStr\\w+".toRegex() !in this && "\\w+$releaseStr\\w*".toRegex() !in this
}