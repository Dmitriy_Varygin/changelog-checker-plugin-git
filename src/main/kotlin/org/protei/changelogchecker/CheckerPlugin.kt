package org.protei.changelogchecker

import org.gradle.api.DefaultTask
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.TaskAction

/**
 * Created by dmitriy_varygin on 29.04.17.
 */
const val CHANGELOG_PATH = "doc/changelog.rst"
const val VERSION_PROPS_PATH = "./version.properties"
const val CRASH_ON_ABSENCE = true
const val DESCRIPTION_REGEX = ".*[\\wа-яА-Я]{5,}.*"
const val CHECK_CHANGELOG_TASK = "checkChangelog"
const val COMPILE_JAVA_TASK = "compileJava"

open class CheckerPlugin : Plugin<Project> {
    override fun apply(project: Project) {
        project.extensions.create("checkChangelogSettings", CheckerPluginExt::class.java)
        val checkerTask = project.tasks.create(CHECK_CHANGELOG_TASK, CheckerTask::class.java)
        val compileJavaTask = project.tasks.find { it.name == COMPILE_JAVA_TASK }
        if (compileJavaTask == null) {
            println("Can't find task '$COMPILE_JAVA_TASK'. You should run task '$CHECK_CHANGELOG_TASK' explicitly " +
                    "or set depends on '$CHECK_CHANGELOG_TASK' for some task.")
        } else {
            compileJavaTask.setDependsOn(setOf(checkerTask))
        }
    }
}

open class CheckerPluginExt(var pathToChangelog: String = CHANGELOG_PATH,
                            var crashOnAbsence: Boolean = CRASH_ON_ABSENCE,
                            var pathToVersionProps: String = VERSION_PROPS_PATH,
                            var descriptionRegex: String = DESCRIPTION_REGEX)

open class CheckerTask : DefaultTask() {
    @TaskAction fun checkChangelog() {
        val settings = project.extensions.findByType(CheckerPluginExt::class.java) ?: CheckerPluginExt()
        check(settings)
    }
}

class CheckChangelogException(override val message: String) : Exception(message)