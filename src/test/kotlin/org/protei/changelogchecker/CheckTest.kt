package org.protei.changelogchecker

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

/**
 * Created by dmitriy_varygin on 01.05.17.
 */
class CheckTest {

    private val validVersionProps = "src/test/resources/valid_version_release.properties"
    private val notValidChangelogPrefix = "src/test/resources/changelog_with_not_valid_243"

    @Test
    fun shouldThrowExcIfChangelogNotExists() {
        assertThrows(CheckChangelogException::class.java,
                { check(CheckerPluginExt("not_exists", true, validVersionProps)) })
    }

    @Test fun shouldGetProperVersionFromValidVersionPropsRelease() {
        val version = getVersion(validVersionProps)
        assertTrue(version.valid())
        assertEquals(2, version.major)
        assertEquals(3, version.middle)
        assertEquals(4, version.minor)
        assertTrue(version.isRelease())
        assertEquals("", version.suffix)
    }

    @Test fun shouldGetProperVersionFromValidVersionPropsSnapshot() {
        val version = getVersion("src/test/resources/valid_version_snapshot.properties")
        assertTrue(version.valid())
        assertEquals(2, version.major)
        assertEquals(1, version.middle)
        assertEquals(175, version.minor)
        assertFalse(version.isRelease())
        assertEquals("SORM_UI-827-SNAPSHOT", version.suffix)
    }

    @Test fun versionReleaseStrShouldReturnRightValue() = assertEquals("1.22.32", Version(1, 22, 32).versionReleaseStr())

    @Test fun stringContainsVersionWorkProperly() {
        val version = Version(1, 22, 3)
        assertTrue(version in "1.22.3")
        assertTrue(version in "  1.22.3")
        assertTrue(version in " 1.22.3  ")
        assertFalse(version in "21.22.35")
        assertFalse(version in "21.22.3   ")
        assertFalse(version in " 1.22.34")
        assertFalse(version in "adfaf1.22.3  a")
    }

    @Test fun shouldValidateProperChangelog() {
        val settings = CheckerPluginExt("src/test/resources/changelog_with_243.rst", true, validVersionProps)
        checkChangelogContainsVersionDescription(settings, Version(2, 4, 3))
    }

    @Test
    fun shouldNotValidateWrongChangelog() {
        val settings = CheckerPluginExt("src/test/resources/changelog_without_243.rst", true, validVersionProps)
        assertThrows(CheckChangelogException::class.java,
                { checkChangelogContainsVersionDescription(settings, Version(2, 4, 3)) })
    }

    @Test
    fun shouldNotValidateWrongChangelog2() {
        val settings = CheckerPluginExt("$notValidChangelogPrefix.rst", true, validVersionProps)
        assertThrows(CheckChangelogException::class.java,
                { checkChangelogContainsVersionDescription(settings, Version(2, 4, 3)) })
    }

    @Test
    fun shouldNotValidateWrongChangelog3() {
        val settings = CheckerPluginExt("${notValidChangelogPrefix}_2.rst", true, validVersionProps)
        assertThrows(CheckChangelogException::class.java,
                { checkChangelogContainsVersionDescription(settings, Version(2, 4, 3)) })
    }

    @Test
    fun shouldNotValidateWrongChangelog4() {
        val settings = CheckerPluginExt("${notValidChangelogPrefix}_3.rst", true, validVersionProps)
        assertThrows(CheckChangelogException::class.java,
                { checkChangelogContainsVersionDescription(settings, Version(2, 4, 3)) })
    }

    @Test
    fun shouldNotValidateWrongChangelogWithCurrentReleaseVersionIsNotAtTheTop() {
        val settings = CheckerPluginExt("${notValidChangelogPrefix}_4.rst", true, validVersionProps)
        assertThrows(CheckChangelogException::class.java,
                { checkChangelogContainsVersionDescription(settings, Version(2, 4, 3)) })
    }
}