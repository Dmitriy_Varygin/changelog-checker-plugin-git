package org.protei.changelogchecker

import org.gradle.api.DefaultTask
import org.gradle.testfixtures.ProjectBuilder
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

/**
 * Created by dmitriy_varygin on 01.05.17.
 */
class CheckerPluginTest {

    private val project = ProjectBuilder.builder().build()

    @BeforeEach
    fun applyPlugin() {
        project.tasks.create(COMPILE_JAVA_TASK, StubCompileJavaTask::class.java)
        project.plugins.apply("changelog-checker")
    }

    @Test
    fun pluginShouldAddTaskToProject() = assertTrue(project.tasks.findByName("checkChangelog") is CheckerTask)

    @Test fun pluginShouldAddExtensionToProject() = assertTrue(
            project.extensions.findByName("checkChangelogSettings") is CheckerPluginExt
    )

    @Test fun extShouldContainsDefaultSettings() {
        val ext = project.extensions.findByName("checkChangelogSettings") as CheckerPluginExt
        assertEquals(CHANGELOG_PATH, ext.pathToChangelog)
        assertTrue(ext.crashOnAbsence)
        assertEquals(DESCRIPTION_REGEX, ext.descriptionRegex)
        assertEquals(VERSION_PROPS_PATH, ext.pathToVersionProps)
    }

    @Test
    fun compileJavaTaskShouldBeDependsOnCheckChangelogTask() {
        val compileJavaTask = project.tasks.find { it.name == COMPILE_JAVA_TASK }
        val checkChangelogTask = compileJavaTask?.dependsOn
                ?.find { it is CheckerTask && it.name == CHECK_CHANGELOG_TASK }
        assertNotNull(checkChangelogTask)
    }

    open class StubCompileJavaTask : DefaultTask()
}
