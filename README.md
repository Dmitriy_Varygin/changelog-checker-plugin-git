**Gradle-плагин для проверки наличия валидного описания релизной версии**

**Настройки**

Автоматически делает таск compileJava зависимым от своего таска checkChangelog.  
Конфигурируется через extension checkChangelogSettings с 4 настройками.    
1. pathToChangelog - путь до changelog'a - по дефолту doc/changelog.rst  
2. crashOnAbsence - бросать ли эксепшен при непрохождении проверки - по дефолту true (при false - выводит в консоль предупреждение)  
3. pathToVersionProps - путь до version.properties - по дефолту ./version.properties  
4. descriptionRegex - regex'a для проверки описания версия - по дефолту .*[\wа-яА-Я]{5,}.* (5 букв)  

Можно задать часть настроек или не задавать их вовсе.  
Пример конфигурации: 

checkChangelogSettings {  
    pathToChangelog './doc/source/changelog.rst'  
    crashOnAbsence true  
    pathToVersionProps './version.properties'  
}  

**Требования к описанию в changelog'e**

- Текущая версия должна быть первой сверху
- Версию от описания должна отделять строчка с, по крайней мере, тремя '-'
- Описание должно состоять из, по крайней мере, пяти букв (при необходимости regex можно изменить)

**История изменений**

1.0.0 

Первая версия плагина

1.0.1

Добавлена проверка на то, что описание текущей релизной версии первая в changelog'e 

1.0.2

Добавлена автоматическая установка зависимости таска compileJava от таска checkChangelog плагина

**Автор**

Дмитрий Варыгин. dmitriy.varygin@yandex.ru